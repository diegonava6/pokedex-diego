import React from 'react';
import PokedexMainContainer from "./containers/Pokedex.main.container.js"

const App = () => {
  return(
      <div>
         <PokedexMainContainer/>
      </div>
  )
}

export default App;