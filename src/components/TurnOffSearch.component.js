import React from "react";

const TurnOffSearch = ({loadPokemons }) => {
  return (
        <button style={{marginBottom: "20px"}} onClick={() => loadPokemons(`https://pokeapi.co/api/v2/pokemon/?offset=0&limit=18`)}>Reset Search</button>
  );
};

export default TurnOffSearch;
