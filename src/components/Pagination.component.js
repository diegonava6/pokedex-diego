import React from "react";

const Pagination = ({ totalPages, activePage, onSelect }) => {
  let pages = [];

  for (let number = 0; number <= totalPages; number++) {
    pages.push(number);
  }

  let pagesRange = pages.slice(
    activePage > 10 ? activePage - 8 : activePage,
    activePage + 10
  );

  let items = pagesRange.map(page => (
    <button
      key={page}
      style={{ fontWeight: activePage === page ? "bold" : "normal" }}
      onClick={() => onSelect(page)}
    >
      {page}
    </button>
  ));

  return (

    <div>
      {activePage > 0 && (
        <button onClick={() => onSelect(activePage - 1)}>Back</button>
      )}
      {items}
      {activePage < totalPages && (
        <button onClick={() => onSelect(activePage + 1)}>Next</button>
      )}
    </div>
  );
};

export default Pagination;
