import React from "react";
import { Card, Col, Container, Row } from "react-bootstrap/";
import {formatNumber, capitalizeFirstLetter} from "../utils/handleStrings.functions"


const PokeList = ({ listOfPokemon, openModal }) => {
  let pokemon = listOfPokemon.map(creature => {
    return (
      creature !== null && (
        <Col sm={3} md={2} xs={4} key={creature.name}>
          <Card
            style={{ margin: "15px 0px" }}
            onClick={openModal.bind(null, creature)}
          >
            <Card.Img
              variant="top"
              src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${creature.url
                .substr(-6, 5)
                .replace(/\D/g, "")}.png`}
            />
            <Card.Title style={{ margin: "15px 0px" }}>
              {capitalizeFirstLetter(creature.name)}
            </Card.Title>
            <h6>
              <small style={{color: "gray"}}>
                #{formatNumber(creature.url.substr(-6, 5).replace(/\D/g, ""), 3)}
              </small>
            </h6>
          </Card>
        </Col>
      )
    );
  });

  return (
    <Container>
      <Row sm={12} md={12}>
        {pokemon}
      </Row>
    </Container>
  );
};

export default PokeList;
