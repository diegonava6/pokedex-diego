import React from "react";
import { Badge } from "react-bootstrap";
import {
  capitalizeFirstLetter,
  formatNumber
} from "../../utils/handleStrings.functions";


const mapWithParameters = variant => item => (
  <Badge key={item.type.name} variant={variant} style={{ marginRight: "10px" }}>
    {item.type.name}
  </Badge>
);

const PokemonDetail = ({ selectedPokemon, variant, dummyText }) => {
  return (
    <React.Fragment>
    <div style={{width: "30%", paddingTop: "40px"}}>
    {<img alt="img" src={selectedPokemon.sprites.front_default} />} 
    </div>
    <div style={{width: "35%", paddingTop: "40px"}}>
      {<small>#{formatNumber(selectedPokemon.id, 3)}</small>}
      <h6 style={{fontWeight: "bold"}}>{capitalizeFirstLetter(selectedPokemon.name)}</h6>
      {selectedPokemon.types.map(mapWithParameters(variant))}
    </div>
    <div style={{display: "flex", flexDirection: "column", paddingTop: "45px"}}>
      {<small style={{paddingBottom: "2px"}}><span style={{fontWeight: "bold"}}>Height:</span> {selectedPokemon.height}m</small>}
      {<small><span style={{fontWeight: "bold"}}>Weight:</span> {selectedPokemon.weight}kg</small>}
    </div>
    <div style={{order: 4, width: "80%"}}>
      <small>{dummyText}</small>
    </div>
    </React.Fragment>
  );
};

export default PokemonDetail;
