import React from 'react';
import {   Radar  } from 'react-chartjs-2';

const PokemonGraph = ({selectedPokemon}) => {
    
    const labels = selectedPokemon.stats.map((info) => {
        return info.stat.name;
    });
 
    const data = selectedPokemon.stats.map((info) => {
        return info.base_stat;
    });
    
    let chartData = {
        labels: labels,
        datasets: [
            {
                label: "Stats",
                data: data,
                pointBackgroundColor: "#5DBCD2",
                borderColor: "#5DBCD2"
                
            }
        ]
    };
    
    return (
        <div>
            <Radar data={chartData}/>
        </div>
    )
}
export default PokemonGraph;