import React from 'react';
import {   HorizontalBar  } from 'react-chartjs-2';

const PokemonGraphLine = ({selectedPokemon}) => {
    
    const labels = selectedPokemon.stats.map((info) => {
        return info.stat.name;
    });
 
    const data = selectedPokemon.stats.map((info) => {
        return info.base_stat;
    });
    
    let chartData = {
        labels: labels,
        datasets: [
            {
                label: "Stats",
                data: data,
                backgroundColor	: "#5DBCD2",
                
            }
        ],
        
    };


    
    
    return (
        <div>
            <HorizontalBar data={chartData} />
        </div>
    )
}
export default PokemonGraphLine;