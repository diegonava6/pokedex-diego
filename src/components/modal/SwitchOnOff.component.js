import React from "react";

const SwitchOnoff = ({ handleGraph }) => {
  return (
    <div style={{alignSelf: "center", marginTop: "35px"}}>
    <small>Chart View &nbsp;</small>
    <label className="switch">
      <input onClick={handleGraph} type="checkbox" />
      <span className="slider round" />
    </label>
    </div>
  );
};

export default SwitchOnoff;
