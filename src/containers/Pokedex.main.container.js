import React, { Component } from "react";
import PokemonList from "./PokemonList.container";
import PokemonModal from "../containers/PokemonModal.container";
import TurnOffSearch from "../components/TurnOffSearch.component"
import Form from 'react-bootstrap/Form'
import logo from "../assets/logo.png";
import "../App.css";

const variantArray = ["primary", "secondary", "success", "danger", "warning", "info", "dark"]
const dummyText = ["Lorem ipsum dolor sit amet, consectetur adipiscing elit.", 
"Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.", 
"Ut enim ad minim veniam.", 
"Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.", 
"Duis aute irure dolor in reprehenderit in voluptate.", 
"Velit esse cillum dolore eu fugiat nulla pariatur.", 
"Excepteur sint occaecat cupidatat non proident.",
"Sunt in culpa qui officia deserunt mollit anim id est laborum."]

class PokedexMainContainer extends Component {
  state = {
    pokemons: [],
    activePage: 0,
    limit: 18,
    offset: 0,
    totalPages: 0,
    count: 0,
    showModal: false,
    showGraph: false,
    selectedPokemon: null,
    variant: null,
    dummyText: null,
    searchFilter: "",
    toogleSearchBar: false
  };

  loadPokemons = url => {
    fetch(url)
      .then(response => {
        return response.json();
      })
      .then(json => {
        let pages = Math.round(json.count / this.state.limit) - 1;

        this.setState({
          pokemons: json.results,
          totalPages: pages,
          count: json.count,
          toogleSearchBar: false
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  searchPokemons = (e) => {
    const {searchFilter} = this.state
      
    if (e.keyCode === 13) {
      fetch("https://pokeapi.co/api/v2/pokemon/?offset=0&limit=1000")
        .then(response => {
          return response.json();
        })
        .then(json => {
          let filteredPokemons = json.results.filter(pokemon => pokemon.name.includes(searchFilter))
          this.setState({
            pokemons: filteredPokemons,
            toogleSearchBar: true
          });
        })
        .catch(err => {
          console.log(err);
        });
    }
  };


  handlePaginationSelect = selectedPage => {
    let offset = this.state.limit * selectedPage;
    this.loadPokemons(
      `https://pokeapi.co/api/v2/pokemon/?offset=${offset}&limit=${
        this.state.limit
      }`
    );
    this.setState({
      activePage: selectedPage
    });
  };

  handleModalOpen = pokemon => {
    if (pokemon.url !== undefined) {
      fetch(`${pokemon.url}`)
        .then(response => {
          return response.json();
        })
        .then(json => {
          this.setState({
            selectedPokemon: json,
            showModal: true,
            variant: variantArray[Math.floor(Math.random()*variantArray.length)],
            dummyText: dummyText[Math.floor(Math.random()*dummyText.length)]
          });
        })
        .catch(err => {
          console.log("parsing failed", err);
        });
    }
  };


  handleGraph = () => {
    this.setState(state => ({ showGraph: !state.showGraph }));
  };

  handleModalClose = () => {
    this.setState({
      showModal: false,
      showGraph: false
    });
  };

  onHandleChange = (e) => {
    const value = e.target.value
    const name = e.target.name

    this.setState({
      [name]: value,
    });
  };

  componentDidMount() {
      this.loadPokemons(
        `https://pokeapi.co/api/v2/pokemon/?offset=${this.state.offset}&limit=${
          this.state.limit
        }`);
    }    

  // componentDidUpdate(prevProps, prevState) {
  //   const {searchFilter, pokemons} = this.state
  //   if (prevState.searchFilter !== searchFilter) {    
  //     this.searchPokemons(`https://pokeapi.co/api/v2/pokemon/?offset=0&limit=1000`, searchFilter);
  //   }
  // }
  

  render() {
    const {
      count,
      pokemons,
      activePage,
      totalPages,
      showModal,
      showGraph,
      selectedPokemon,
      variant,
      dummyText,
      toogleSearchBar,
      searchFilter
    } = this.state;

    console.log("this.state: ", this.state)

    return (
      <div className="App">
        <img className="Logo" src={logo} width="150vh" alt="logo" />

        <div style={{display: "flex", flexDirection: "column", paddingBottom: "20px"}}>
          <Form.Control style={{alignSelf: "center", width: "50%"}} name="searchFilter" onKeyDown={this.searchPokemons} onChange={this.onHandleChange} value={searchFilter} type="name" placeholder="Search by keywords..." />
          {toogleSearchBar && <div style={{alignSelf: "center", width: "10%", marginTop: "10px"}}><TurnOffSearch loadPokemons={this.loadPokemons}/> </div>}
        </div>

        <PokemonList
          allValue={count}
          listOfPokemon={pokemons}
          activePage={activePage}
          onSelect={this.handlePaginationSelect}
          totalPages={totalPages}
          openModal={this.handleModalOpen}
          toogleSearchBar={toogleSearchBar}
          loadPokemons={this.loadPokemons}
        />

        <PokemonModal
          closeModal={this.handleModalClose}
          showModal={showModal}
          showGraph={showGraph}
          selectedPokemon={selectedPokemon}
          handleGraph={this.handleGraph}
          variant={variant}
          dummyText={dummyText}
        />
      </div>
    );
  }
}

export default PokedexMainContainer;
