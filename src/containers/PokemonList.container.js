import React from "react";
import PokeList from "../components/PokemonList.component";
import Pagination from "../components/Pagination.component";
import TurnOffSearch from "../components/TurnOffSearch.component";

const PokemonList = ({
  listOfPokemon,
  offset,
  btnSize,
  totalPages,
  activePage,
  onSelect,
  openModal,
  toogleSearchBar,
  loadPokemons
}) => {
  return (
    <div>
      <PokeList listOfPokemon={listOfPokemon} openModal={openModal} />

      {!toogleSearchBar ? <Pagination
        btnSize={btnSize}
        totalPages={totalPages}
        activePage={activePage}
        onSelect={onSelect}
        offset={offset}
      /> : <TurnOffSearch loadPokemons={loadPokemons}/>
      }    
    </div>
  )
}

export default PokemonList;
