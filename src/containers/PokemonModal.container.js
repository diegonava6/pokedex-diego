import React from "react";
import { Modal } from "react-bootstrap";
import PokemonGraphRadar from "../components/modal/graphs/PokemonGraphRadar.component";
import PokemonGraphLine from "../components/modal/graphs/PokemonGraphLine.component";
import PokemonDetail from "../components/modal/PokemonDetail.component";
import SwitchOnoff from "../components/modal/SwitchOnOff.component";

const PokemonModal = ({
  closeModal,
  showModal,
  selectedPokemon,
  showGraph,
  handleGraph,
  variant,
  dummyText
}) => {
  return (
    <div>
      {console.log("pokemon data on modal: ", selectedPokemon)}
      <Modal show={showModal} onHide={closeModal}>
        <Modal.Header closeButton style={{flexWrap: "wrap"}}>
          <PokemonDetail dummyText={dummyText} variant={variant} selectedPokemon={selectedPokemon} />
        </Modal.Header>
        <Modal.Body style={{display: "flex", flexFlow: "column"}}>
          {selectedPokemon !== null && showGraph ? (
            <PokemonGraphRadar selectedPokemon={selectedPokemon} />
          ) : (
            <PokemonGraphLine selectedPokemon={selectedPokemon} />
          )}
          <SwitchOnoff handleGraph={handleGraph} />
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default PokemonModal;
