import React from 'react';
import ReactDOM from 'react-dom';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App from '../App';
import TurnOffSearch from "../components/TurnOffSearch.component"
import SwitchOnoff from "../components/modal/SwitchOnoff.component"



configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});


it("TurnOff Search Component renders correctly", () => {
  const wrapper = shallow(
    <TurnOffSearch />
  );
  expect(wrapper).toMatchSnapshot();
});


it("SwitchOnoff renders correctly", () => {
  const wrapper = shallow(
    <SwitchOnoff />
  );
  expect(wrapper).toMatchSnapshot();
});


